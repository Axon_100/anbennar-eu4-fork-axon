# No previous file for Nakhchivan

owner = S83
controller = S83
add_core = S83
 
culture = kombedak
religion = beast_memory
capital = ""

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = fish
add_permanent_province_modifier = {
    name = lizardfolk_minority_coexisting_small
    duration = -1
}