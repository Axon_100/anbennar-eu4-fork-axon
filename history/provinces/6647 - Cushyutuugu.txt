# No previous file for Banten
owner = S31
controller = S31
add_core = S31
culture = wakoodak
religion = beast_memory
trade_goods = silk
hre = no
base_tax = 2
base_production = 3
base_manpower = 2
native_size = 40
native_ferocity = 8
native_hostileness = 4

is_city = yes